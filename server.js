var winston = require('winston');
var io = require('socket.io')(60606);
var keypress = require('keypress');
var fs = require('fs');

winston.level = 'debug';
winston.add(winston.transports.File, { filename: 'rainring.log',  json: false });
winston.log('info', 'Rainring Server started...');

io.on('connection', function (socket) {
  var address = socket.handshake.address;
  winston.log('info', 'New connect from ' + address);
});

// check if on rasperry
fs.readFile('/etc/os-release', 'UTF8', function (err,data) {
  if (err) {
    winston.log('error', 'Looks like we are not on the RPI...');

    return;
  } else {
    winston.log('info', data );

    // the real rpi thing
    var gpio = require('rpi-gpio');
    gpio.setup(7, gpio.DIR_IN, gpio.EDGE_RISING);

    gpio.on('change', function(channel, value) {
        if(value == true){
        winston.log('info', 'Person detected...');
        io.emit('ring', { hello: 'world' });
        }
    });
  }
});

// simulate with keypress
keypress(process.stdin);
process.stdin.on('keypress', function (ch, key) {
  if (key && key.ctrl && key.name == 'c') {
    process.exit();
  } else {
    io.emit('ring', { hello: 'world' });
    winston.log('info', 'simulated...');

  }
});
process.stdin.setRawMode(true);
process.stdin.resume();