var winston = require('winston');
var notifier = require('node-notifier');
var path = require('path');
var io = require('socket.io-client');

winston.level = 'debug';
winston.add(winston.transports.File, { filename: 'rainring.log',  json: false });
winston.log('info', 'Rainring Client started...');

socket = io.connect('http://raspberrypi-alex.local:60606');

socket.on('connect', function () { 
  winston.log('info', 'connected to RPI');
});

socket.on('disconnect', function(){
  winston.log('info', 'Disconnected from RPI');
});

socket.on('ring', function(data){
  winston.log('info', 'Pssst... looks like a new customer is here!');
  notifier.notify({
    'title': 'T€MPORAR¥ PARADI$€',
    'message': 'Pssst... looks like a new customer is here!',
    icon: path.join(__dirname, 'icon.png'), 
    sound: 'Ping',
    wait: true // Wait with callback, until user action is taken against notification
  });
});




